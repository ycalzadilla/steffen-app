import service from '../../services/clientsService'

export default {
  state: {
    clients: [],
    totalClients: 0,
    loadedClients: false
  },
  getters: {
    getClients: state => {
      return state.clients
    },
    getClientsLength: state => {
      return state.totalClients
    },
    getLoadedClients: state => {
      return state.loadedClients
    }
  },
  mutations: {
    SET_CLIENTS_LIST (state, payload) {
      if (payload.data.data !== undefined) {
        state.clients = payload.data.data
        state.totalClients = payload.data.total
      }
      state.loadedClients = true
    },
    SET_LOADING_CLIENTS (state) {
      state.loadedClients = false
    }
  },
  actions: {
    setClientsList ({ commit }, payload) {
      commit('SET_LOADING_CLIENTS')

      let dataParams = {}
      let searchClient = false
      for (let key in payload.pageSetup) {
        dataParams[key] = payload.pageSetup[key]
      }

      if (payload.searchData) {
        searchClient = true
        dataParams['search'] = payload.searchData
      }

      if (searchClient === false) {
        return service.getAllClients(dataParams)
          .then(response => {
            commit('SET_CLIENTS_LIST', { data: response.data })
            return response
          })
          .catch(err => commit('FETCHING_DATA_ERROR', err))
      } else {
        return service.searchClients(dataParams)
          .then(response => {
            commit('SET_CLIENTS_LIST', { data: response.data })
            return response
          })
          .catch(err => commit('FETCHING_DATA_ERROR', err))
      }
    }
  }
}
